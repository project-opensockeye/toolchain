# Unfortunately, we are stuck with amd64 for the forseeable future, since
# z3 4.8.5 is broken on arm64 platforms.
#
# The FStar devs are aware of this, but don't really care [1]. Once this is
# fixed we can drop the platform here and have fast native builds for arm
# machines.
#
# [1]: https://github.com/FStarLang/FStar/issues/2332
FROM --platform=linux/amd64 ubuntu:22.04
WORKDIR /tools

RUN apt-get update && apt-get install --no-install-recommends -y \
  ca-certificates \
  git \
  libgmp-dev \
  libgomp1 \
  make \
  opam \
  python2 \
  vim-tiny \
  && rm -rf /var/lib/apt/lists/*

RUN opam init --bare --disable-sandboxing
RUN opam switch create 4.14.1
RUN echo 'eval $(opam env)' >> /root/.bashrc
RUN opam pin -y fstar.2023.03.22 --dev-repo

RUN apt-get update && apt-get install --no-install-recommends -y \
  curl \
  && rm -rf /var/lib/apt/lists/*
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:$PATH"

WORKDIR /build
VOLUME /build
