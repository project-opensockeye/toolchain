# OS4RC toolchain

This docker image contains all you need to compile and run our OS4RC toolchain.
OS4RC is under active development, so changes might occur to this image.

We may be able to provide an image via a registry on our end at some point, for
now just clone this repo and then run `docker build -t os4rc-toolchain .`.

Unfortunately, due to shenanigans in the software we use, we only support the
amd64 architecture right now. For MacOS M1 users, docker provides automatic
emulation, be sure to also enable Rosetta in the advanced settings for massive
speedups!
